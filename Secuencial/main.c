#include <stdio.h>
#include <stdlib.h>
#include "leearchivo.c"
#include "vectores.c"
#include <sys/resource.h>
#include <time.h>
#include <sys/time.h>

int main(int argc, char *argv[])
{
	float **bd, **consultas, rango;
		char *rutaDB, *rutaConsulta;
		int i,dim, numElement, numConsultas, *resp;

		//Variables para medir el tiempo:
	   struct rusage r1, r2;
	   float user_time, sys_time, real_time;
	   struct timeval t1, t2;


		if(argc<6)
		{
			printf("ingrese: ./main.out BD numElement consultas numConsultas dim rango\n");
			return 1;
		}

		rutaDB=argv[1];
		numElement=atoi(argv[2]);
		rutaConsulta=argv[3];
		numConsultas=atoi(argv[4]);
		dim=atoi(argv[5]);
		rango = atof(argv[6]);
		bd = (float **)malloc(sizeof(float **)*numElement);
		for(i=0;i<numElement;i++)
			bd[i] = (float *)malloc(sizeof(float *)*dim);
		scanFile(rutaDB, bd,dim, numElement);

		consultas=(float **)malloc(sizeof(float **)*numConsultas);
		for(i=0;i<numConsultas;i++)
			consultas[i] = (float *)malloc(sizeof(float *)*dim);
		scanFile(rutaConsulta, consultas, dim, numConsultas);
		
		resp = (int *)malloc(sizeof(int *)*numConsultas);
		
		printf("procesando\n");
		fflush(stdout);
		//Inicio de la medida del tiempo
	     gettimeofday(&t1, 0);
	     getrusage(RUSAGE_SELF, &r1);

		for(i=0;i<numConsultas;i++)
			resp[i] = consultarPorRango(bd, numElement, consultas[i], dim, rango);
		
	     //Fin de la medida del tiempo
	     gettimeofday(&t2, 0);
	     getrusage(RUSAGE_SELF, &r2);
	     

		for(i=0;i<numConsultas;i++)
			printf("C%d = %d\n",(i+1),resp[i]);

		//Obteniendo el tiempo
	   	 user_time = (r2.ru_utime.tv_sec - r1.ru_utime.tv_sec) + (r2.ru_utime.tv_usec - r1.ru_utime.tv_usec)/1000000.0;
	     sys_time = (r2.ru_stime.tv_sec - r1.ru_stime.tv_sec) + (r2.ru_stime.tv_usec - r1.ru_stime.tv_usec)/1000000.0;
	     real_time = (t2.tv_sec - t1.tv_sec) + (float)(t2.tv_usec - t1.tv_usec)/1000000;

	     printf("\nCPU Time = %f", sys_time + user_time);
     printf("\nReal Time = %f\n", real_time);

     return 0;
}
