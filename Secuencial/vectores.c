#include <omp.h>
#include <math.h>

float distancia(float *p1, float *p2, int DIM);
int consultarPorRango(float **datos, int numElement, float *consulta, int dim, float rango);
float distancia(float *p1, float *p2, int DIM)
{
    int i=0;
    long double suma=0;

	for (i=0; i < DIM; i++)
		suma += ((p1[i]-p2[i])*(p1[i]-p2[i]));
    
	return sqrtf(suma);
}
int consultarPorRango(float **datos, int numElement,float *consulta,int dim, float rango)
{
    int threads,thread, i, suma = 0;

    for(i=0;i<numElement;i++)
        if(distancia(datos[i],consulta,dim)<=rango)
                suma++;
    
        return suma;
}
