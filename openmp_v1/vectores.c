#include <omp.h>
#include <math.h>

float distancia(float *p1, float *p2, int DIM);
int consultarPorRango(int num_threads,float **datos, int num_element, float *consulta, int dim, float rango);
float distancia(float *p1, float *p2, int DIM)
{
    int i=0;
    long double suma=0;
	for (i=0; i < DIM; i++)
		suma += ((p1[i]-p2[i])*(p1[i]-p2[i]));
    
	return sqrtf(suma);
}
int consultarPorRango(int num_threads, float **datos, int num_element,float *consulta,int dim, float rango)
{	
	int thread_num, i, suma = 0, *suma_local;
	suma_local = (int *)malloc(sizeof(int)*num_threads);
	for(i=0;i<num_threads;i++)
		suma_local[i] = 0;
	
	#pragma omp parallel for
        	for(i=0;i<num_element; i++)
            		if(distancia(datos[i], consulta, dim)<=rango)
                		suma_local[omp_get_thread_num()]++;
    	
	for(i=0;i<num_threads;i++)
		suma += suma_local[i];
	
	return suma;
}
