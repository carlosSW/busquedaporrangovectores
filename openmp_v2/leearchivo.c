#include <stdio.h>
#include <stdlib.h>
#define ERROR -1

void scanFile(char *ruta, float **datos, int DIM, int numElement);
int leedato(float *dato,FILE *file, int DIM);
void copiavalor(float *a, float *b, int DIM);

void scanFile(char *ruta, float **datos, int DIM, int numElement)
{	
	float dato[DIM];
	int i;
	FILE *f_dist;

	printf("\nAbriendo %s... ", ruta);
	
	f_dist = fopen(ruta, "r");
	printf("OK\n");
	fflush(stdout);
	
   printf("\nLeyendo DB... ");
	fflush(stdout);
    for (i=0; i<numElement; i++)
    {
       if (leedato(dato, f_dist, DIM)==ERROR || feof(f_dist))//Si hay un error o se alcanzo el final del archivo
       {
          printf("\n\nERROR :: N_DB mal establecido\n\n");
          fflush(stdout);
          fclose(f_dist);
          break;
       }
       copiavalor(datos[i], dato, DIM);
    }
    fclose(f_dist);//Cerrando el archivo que se abrio con fopen
    printf("OK\n");
	
    fflush(stdout);
}

int leedato(float *dato,FILE *file, int DIM)
{
   int i=0;
   
   for (i=0; i < DIM; i++)
      if (fscanf(file, "%f,", &(dato[i])) < 1)
         return ERROR;

   return 1;
}

void copiavalor(float *a, float *b, int DIM)
{
   int i;
   for (i=0; i<DIM; i++)
      a[i] = b[i];
   return;
}
