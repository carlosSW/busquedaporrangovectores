#include <omp.h>
#include <math.h>

float distancia(float *p1, float *p2, int DIM);
int consultarPorRango(float **datos, int num_element, float *querie, int dim, float range);
void imprimir(float *vector, int dim);
float distancia(float *p1, float *p2, int DIM)
{
    int i=0;
    long double suma=0;

	for (i=0; i < DIM; i++)
		suma += ((p1[i]-p2[i])*(p1[i]-p2[i]));
    
	return sqrtf(suma);
}
int consultarPorRango(float **datos, int num_element,float *querie,int dim, float range)
{
    int  i, suma = 0;

	for(i=0;i<num_element;i++)
		if(distancia(datos[i],querie,dim)<=range)
        		suma++;
	
        return suma;
}
void imprimir(float *vector, int dim)
{
	int i;
	for(i=0;i<dim;i++)
		printf("%f", vector[i]);
	printf("\n");
}
