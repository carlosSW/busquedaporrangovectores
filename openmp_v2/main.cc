#include <stdio.h>
#include <stdlib.h>
#include "leearchivo.c"
#include "vectores.c"
#include <sys/resource.h>
#include <time.h>
#include <sys/time.h>

int main(int argc, char *argv[])
{
	float **db, **queries, range;
		char *ruta_db, *ruta_queries;
		int i,dim, num_element, num_queries, *resp, num_threads, thread_num;

	//Variables para medir el tiempo:
	struct rusage r1, r2;
	float user_time, sys_time, real_time;
	struct timeval t1, t2;

	if(argc<6)
	{
		printf("ingrese: ./main.out num_threads BD numElement consultas numConsultas dim rango\n");
		return 1;
	}
	
	num_threads=atoi(argv[1]);
	ruta_db = argv[2];
	num_element = atoi(argv[3]);
	ruta_queries = argv[4];
	num_queries = atoi(argv[5]);
	dim = atoi(argv[6]);
	range = atof(argv[7]);
	//estableciendo el numero de hilos	
	omp_set_num_threads(num_threads);
	//leyendo base de datos
	db = (float **)malloc(sizeof(float *)*num_element);
		for(i=0;i<num_element;i++)
			db[i] = (float *)malloc(sizeof(float)*dim);
	scanFile(ruta_db, db, dim, num_element);
	
	//leyendo consultas
	queries=(float **)malloc(sizeof(float *)*num_queries);	
	for(i=0;i<num_queries;i++)
		queries[i] = (float *)malloc(sizeof(float)*dim);
	scanFile(ruta_queries, queries, dim, num_queries);
		
	resp = (int *)malloc(sizeof(int *)*num_queries);
	for(i=0; i<num_queries;i++)
		resp[i] = 0;
	printf("procesando\n");
	fflush(stdout);
	//Inicio de la medida del tiempo
	gettimeofday(&t1, 0);
	getrusage(RUSAGE_SELF, &r1);
	#pragma omp parallel for
		for(i=0; i < num_queries; i++)
			resp[i] = consultarPorRango(db, num_element, queries[i], dim, range);
	
	//Fin de la medida del tiempo
	gettimeofday(&t2, 0);
	getrusage(RUSAGE_SELF, &r2);

	for(i=0;i<num_queries;i++)
		printf("Qurie %d = %d\n",(i+1),resp[i]);

	//Obteniendo el tiempo
	user_time = (r2.ru_utime.tv_sec - r1.ru_utime.tv_sec) + (r2.ru_utime.tv_usec - r1.ru_utime.tv_usec)/1000000.0;
	sys_time = (r2.ru_stime.tv_sec - r1.ru_stime.tv_sec) + (r2.ru_stime.tv_usec - r1.ru_stime.tv_usec)/1000000.0;
	real_time = (t2.tv_sec - t1.tv_sec) + (float)(t2.tv_usec - t1.tv_usec)/1000000;
	
	printf("\nCPU Time = %f", sys_time + user_time);
     	printf("\nReal Time = %f\n", real_time);

     return 0;
}
